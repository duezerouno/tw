<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdn.jsdelivr.net/npm/swiper@4.3.5/dist/js/swiper.min.js"></script>
		<?php wp_head(); ?>

		<link rel="stylesheet" href="https://unpkg.com/tachyons@4.9.1/css/tachyons.min.css"/>
		<link href="https://fonts.googleapis.com/css?family=Rozha+One" rel="stylesheet">

		<style>
		.animsition-loading {
		  background-image: url('<?php echo get_template_directory_uri(); ?>/img/food.gif');
		  background-size: contain;
		  width: 140px;
		  height: 140px;
		}
		</style>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="animsition">

			<!-- header -->
			<header class="fixed top-0 w-100 z-5 hide-mobile">
				<div class="tess_navigation">
				  <div class="cf ph2-ns">
				    <div class="fl w-100 w-20-ns">
				      <div class="item pv3 tc"><a href="<?php echo get_site_url(); ?>/recipes">Recipes</a></div>
				    </div>
				    <div class="fl w-100 w-20-ns">
				      <div class="item pv3 tc"><a href="<?php echo get_site_url(); ?>/about">About</a></div>
				    </div>
				    <div class="fl w-100 w-20-ns">
				      <div class="pv3 tc logo"><a href="<?php echo get_site_url(); ?>">TESS WARD</a></div>
				    </div>
				    <div class="fl w-100 w-20-ns">
				      <div class="item pv3 tc"><a href="<?php echo get_site_url(); ?>/events">Events</a></div>
				    </div>
						<div class="fl w-100 w-10-ns">
						 <div class="item pv3 tc"><a href="<?php echo get_site_url(); ?>/contact">Contact</a></div>
					 </div>
					 <div class="fl w-100 w-10-ns">
						<div class="item pv3 tc"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/lens.svg" class="js-trigger-overlay"></div>
					</div>
				  </div>
				</div>
			</header>
			<!-- /header -->

			<!-- mobile header -->
			<header class="fixed top-0 w-100 z-5 hide-desktop">
				<div class="tess_navigation">
				  <div class="cf ph2-ns">
				    <div class="fl w-20">
				      <div class="item pv3 tc"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/lens.svg" class="js-trigger-overlay"></div>
				    </div>
				    <div class="fl w-60">
				      <div class="pv3 tc logo">TESS WARD</div>
				    </div>
						<div class="fl w-20">
							<div class="item pv3 tc"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/burger_menu.svg" class="js-trigger-menu burger-menu"></div>
						</div>
				  </div>
				</div>
			</header>
			<!-- /mobile-header -->
