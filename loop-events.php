<div class="preslide_divider">
	<span class="separate_border tess-mid-pink-bg"></span>
	<span class="separate_border tess-sky-blue-bg"></span>
	<span class="separate_border tess-yellow-bg"></span>
	<span class="separate_border tess-orange-bg"></span>
	<span class="separate_border tess-dark-pink-bg"></span>
	<span class="separate_border tess-green-bg"></span>
</div>
<!-- Slider main container -->
<div class="swiper-container">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			<div class="swiper-slide">
				<article class="vh-75 vh-50-ns dt w-100">
					<div class="dtc v-mid tc white">
						<div class="dt center ph4">
							<div class="db dtc-ns v-mid w-100">
								<h1 class="tess-light-blue">‘I specialise in connecting brands and people through immersive food experiences’</h1>
							</div>
						</div>
					</div>
				</article>
			</div>
			<div class="swiper-slide">
				<article class="vh-75 vh-50-ns dt w-100">
					<div class="dtc v-mid tc white">
						<div class="dt center ph4">
							<div class="db dtc-ns v-mid w-100">
								<h1 class="tess-yellow">‘Food is more than what is on the plate and should offer something interesting and impactful’</h1>
							</div>
						</div>
					</div>
				</article>
			</div>
			<div class="swiper-slide">
				<article class="vh-75 vh-50-ns dt w-100">
					<div class="dtc v-mid tc white">
						<div class="dt center ph4">
							<div class="db dtc-ns v-mid w-100">
								<h1 class="tess-dark-pink">‘My speciality is in experiential dining experiences and events’</h1>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
		<!-- If we need pagination -->
		<div class="swiper-pagination"></div>
</div>

<div class="preslide_divider pt3">
	<span class="separate_border tess-mid-pink-bg"></span>
	<span class="separate_border tess-sky-blue-bg"></span>
	<span class="separate_border tess-yellow-bg"></span>
	<span class="separate_border tess-orange-bg"></span>
	<span class="separate_border tess-dark-pink-bg"></span>
	<span class="separate_border tess-green-bg"></span>
</div>


<div class="container is-fluid pt4">

	<div class="dt center ph4 pb4">
		<div class="db dtc-ns v-mid w-100">
			<h1 class="tess-orange tc">Events &  Consultancy Projects</h1>
		</div>
	</div>

	<div class="recipes" id="recipes">
		<div class="recipe-sizer"></div>
		<div class="recipe-gutter"></div>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<div <?php post_class( 'recipe-item' ); ?>>

				<!-- post thumbnail -->
				<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
						<p class="pb4"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
					</a>
				<?php endif; ?>
				<!-- /post thumbnail -->

			</div>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

		<?php endif; ?>
	</div>
</div>
