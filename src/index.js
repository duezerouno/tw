import 'jquery';
import 'popper.js';
import 'bootstrap';
import 'animsition';
import imagesLoaded from 'imagesloaded';
import AOS from 'aos';

import './sass/style.scss';

import './stick.js';
import './marquee.js';
import './packery.js';

var isotope = require('isotope-layout');

'use strict';

$('.stick').fixTo('.lastpic', {
    top: 120
});

$(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in',
    outClass: 'fade-out',
    inDuration: 3000,
    outDuration: 3000,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
});

$('.marquee').marquee({
	direction: 'left',
  startVisible: true,
	duplicated:true,
	pauseOnHover:true,
	duration:55000,
	delayBeforeStart:100,
	gap:0
});


imagesLoaded( document.querySelector('#secondgrid'), function( instance ) {
	$('.second-grid').packery({
		itemSelector: '.second-grid-item',
		columnWidth: '.second-grid-sizer',
		gutter: '.second-gutter-sizer',
		percentPosition: true,
		originLeft: true
	});
});

imagesLoaded( document.querySelector('#grid'), function( instance ) {
	$('.grid').packery({
		itemSelector: '.grid-item',
		columnWidth: '.grid-sizer',
		gutter: '.gutter-sizer',
		percentPosition: true,
		originLeft: false
	});
});

imagesLoaded( document.querySelector('#recipes'), function( instance ) {
  $('.recipes').isotope({
    // options
    itemSelector: '.recipe-item',
    percentPosition: true,
    layoutMode: 'fitRows',
      fitRows: {
        gutter: '.recipe-gutter'
      }
  });
});


var mySwiper = new Swiper ('.swiper-container', {
	loop: true,
	speed: 800,
	grabCursor: true,
	preventClicks: false,
	pagination: {
    el: '.swiper-pagination',
		clickable: true
  },
	mousewheel: {
    invert: true,
		forceToAxis: true,
  }
})


$('.js-trigger-overlay').on('click', function () {
	 $("#overlay").fadeToggle(600);
});

$('.search-submit').on('click', function () {
	 $("#overlay").fadeToggle(600);
});

$('.js-trigger-menu').on('click', function () {
	 $("#menu-overlay").fadeToggle(600);
});

$('.js-close-menu').on('click', function () {
	 $("#menu-overlay").fadeToggle(600);
});
