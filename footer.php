			<!-- footer -->
			<footer class="tess-mid-pink-bg pv5 pv0-ns">
				<article class="vh-15 dt w-100">
				  <div class="dtc v-mid tc tl-ns">
						<div class="center">
						  <div class="container is-fluid cf">
						    <div class="fl w-100 w-50-ns pv5 pv0-ns">
						      <h2 class="tess-orange dib pb4 pb0-ns f15">Want delicious, wholesome food?</h2>
									<span class="dib">
										<input class="mw-100 w-100 w5-ns f5">
	      						<input type="submit" class="input-reset w-100 w-auto-ns f5 pt4 pt0-ns f11" value="Subscribe">
									</span>
						    </div>
						    <div class="fl w-100 w-50-ns">
									<div class="w-100">
								    <ul id="horizontal-style">
							        <li class="f13"><a href="#">Legal</a></li>
							        <li class="f13"><a href="#">Facebook</a></li>
							        <li class="f13"><a href="#">Instagram</a></li>
							        <li class="f13"><a href="#">Twitter</a></li>
							        <li class="f13"><a href="#">Youtube</a></li>
											<li class="f9"><a href="#">Site Designed by The Digital Fairy</a></li>
								    </ul>
									</div>
						    </div>
						  </div>
						</div>
				  </div>
				</article>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
