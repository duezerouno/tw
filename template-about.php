<?php /* Template Name: About Page Template */ get_header(); ?>
<!-- About Page Template  -->
<?php get_template_part( 'overlay' ); ?>
<main>
	<article class="vh-100 dt w-100 tess-light-pink-bg">
	  <div class="ph3 ph4-l">
			<div class="w-100 w-50-ns center about_absolute">
		    <h1 class="tess-orange tc">Le Cordon Bleu trained chef, food writer and author</h1>
				<p class="tess-orange tl lh-copy">
					I have a passion for all things edible and aim to help people get to grips with produce-led, colourful dishes.
					Being some of my favourite foods to eat, my recipes have always has a strong influence from eastern Mediterranean and North African cuisine, I love the way spices and herbs can transform even the most simple of ingredients into something exciting, the endless inspiration I get from the ripe, sun drenched produce and the way that meals are a celebration to relish and take time over. My absolute aim is always to create food that you are excited to make and eat. Dishes that delight the tastebuds, with colourful ingredients that support happiness and good health. My first cookbook ‘The Naked Diet’ does just this – celebrating unprocessed ‘naked’ ingredients and stripped back cooking with a riot of delicious dishes that count colours to nourish body and tastebuds. While running tessward.com, I also work as a creative consultant, freelance food and travel writer. Brands I have worked with include Fortnum and Mason, Itsu, Grey Goose, Sainsbury’s,  Panasonic and Kallo. I have also worked in the kitchens at River Cottage, The Ritz, and often host VIP dinners for clients such as Alice Temperley, Michael Kors, Mulberry and other brands.
				</p>
			</div>
	  </div>
	</article>
</main>
