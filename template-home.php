<?php /* Template Name: Home Page Template */ get_header(); ?>
<!-- Home Page Template  -->
<?php get_template_part( 'overlay' ); ?>
<main>
	<article class="w-100 tess-light-pink-bg">

		<section class="marquee_container">
		   <nav class="flex pv3">
		     <div class="marquee ttu" data-direction="left">
		       <span><a class="" href="/recipes"><span class="tess-yellow">YUMMY</span> <span class="tess-orange">AND</span> <span class="tess-indigo">HEALTHY</span> <span class="tess-dark-pink">RECIPES</span> </a></span>
					 <span><a class="" href="/recipes"><span class="tess-green">YUMMY</span> <span class="tess-yellow">AND</span> <span class="tess-dark-blue">HEALTHY</span> <span class="tess-red">RECIPES</span> </a></span>
					 <span><a class="" href="/recipes"><span class="tess-violet">YUMMY</span> <span class="tess-red">AND</span> <span class="tess-indigo">HEALTHY</span> <span class="tess-dark-pink">RECIPES</span> </a></span>
					 <span><a class="" href="/recipes"><span class="tess-yellow">YUMMY</span> <span class="tess-orange">AND</span> <span class="tess-indigo">HEALTHY</span> <span class="tess-dark-pink">RECIPES</span> </a></span>
					 <span><a class="" href="/recipes"><span class="tess-green">YUMMY</span> <span class="tess-yellow">AND</span> <span class="tess-dark-blue">HEALTHY</span> <span class="tess-red">RECIPES</span> </a></span>
					 <span><a class="" href="/recipes"><span class="tess-violet">YUMMY</span> <span class="tess-red">AND</span> <span class="tess-indigo">HEALTHY</span> <span class="tess-dark-pink">RECIPES</span> </a></span>
		     </div>
		   </nav>
		 </section>

		 <div class="container is-fluid pt3-ns db">
 			<div class="second-grid" id="secondgrid">
 				<div class="second-gutter-sizer"></div>
 				<div class="second-grid-sizer"></div>
 				<div class="second-grid-item"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
 					<div class="dt w-100 recipe_title">
 					 <div class="dtc">
 						 <h2 class="f5 f4-ns">Title</h2>
 					 </div>
 					 <div class="dtc tr">
 						 <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
 					 </div>
 				 </div>
 				</div>
 				<div class="second-grid-item second-grid-item--width2"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
 					<div class="dt w-100 recipe_title">
 					 <div class="dtc">
 						 <h2 class="f5 f4-ns">Title</h2>
 					 </div>
 					 <div class="dtc tr">
 						 <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
 					 </div>
 				 </div>
 				</div>
 				<div class="second-grid-item"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
 					<div class="dt w-100 recipe_title">
 					 <div class="dtc">
 						 <h2 class="f5 f4-ns">Title</h2>
 					 </div>
 					 <div class="dtc tr">
 						 <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
 					 </div>
 				 </div>
 				</div>
 		 </div>
 	 </div>

		 <div class="container is-fluid pb3 pt5-ns pb5-ns db">
			 <div class="grid" id="grid">
				 <div class="gutter-sizer"></div>
				 <div class="grid-sizer"></div>
				 <div class="grid-item"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
					 <div class="dt w-100 recipe_title">
			      <div class="dtc">
			        <h2 class="f5 f4-ns">Title</h2>
			      </div>
			      <div class="dtc tr">
			        <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
			      </div>
			    </div>
				 </div>
				 <div class="grid-item grid-item--width2"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
					 <div class="dt w-100 recipe_title">
			      <div class="dtc">
			        <h2 class="f5 f4-ns">Title</h2>
			      </div>
			      <div class="dtc tr">
			        <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
			      </div>
			    </div>
				 </div>
				 <div class="grid-item"><img src="<?php echo get_template_directory_uri(); ?>/img/pork.jpg">
					 <div class="dt w-100 recipe_title">
			      <div class="dtc">
			        <h2 class="f5 f4-ns">Title</h2>
			      </div>
			      <div class="dtc tr">
			        <h2 class="f5 mv0 recipe_arrow fr tr"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="dib"></h2>
			      </div>
			    </div>
				 </div>
	 	 	</div>
		</div>

		<div class="preslide_divider pv3">
			<span class="separate_border tess-mid-pink-bg"></span>
			<span class="separate_border tess-sky-blue-bg"></span>
			<span class="separate_border tess-yellow-bg"></span>
			<span class="separate_border tess-orange-bg"></span>
			<span class="separate_border tess-dark-pink-bg"></span>
			<span class="separate_border tess-green-bg"></span>
		</div>


		<!-- Slider main container -->
		<div class="swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
	        <!-- Slides -->
	        <div class="swiper-slide">
						<article class="vh-75 vh-50-ns dt w-100">
						  <div class="dtc v-mid tc white">
								<div class="dt mw8 center">
								  <div class="db dtc-ns v-mid-ns slide_picture">
								    <img src="<?php echo get_template_directory_uri(); ?>/img/thenakeddiet.jpg" />
								  </div>
								  <div class="db dtc-ns v-mid w-50-ns">
										<h2 class="ttu tess-light-blue">Tess Ward</h2>
										<h1 class="ttu tess-light-blue lh-solid">The Naked Diet</h1>
										<p class="tess-light-blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
										<h2 class="ttu tess-light-blue pv3"><a href="#">Order Now</a></h2>
								  </div>
								</div>
						  </div>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="vh-75 vh-50-ns dt w-100">
						  <div class="dtc v-mid tc white">
								<div class="dt mw8 center">
								  <div class="db dtc-ns v-mid-ns slide_picture">
								    <img src="<?php echo get_template_directory_uri(); ?>/img/event.jpg" />
								  </div>
								  <div class="db dtc-ns v-mid w-50-ns">
										<h2 class="ttu tess-light-blue">MON 28 MAY -  17:00</h2>
										<h1 class="ttu tess-light-blue lh-solid">Heartwarming Valentines Yoga Supperclub</h1>
										<p class="tess-light-blue">Yoga point, London</p>
										<h2 class="ttu tess-light-blue pv3"><a href="#">Book Now</a></h2>
								  </div>
								</div>
						  </div>
						</article>
					</div>
		    </div>
		    <!-- If we need pagination -->
		    <div class="swiper-pagination"></div>
		</div>

		<div class="preslide_divider pt3">
			<span class="separate_border tess-mid-pink-bg"></span>
			<span class="separate_border tess-sky-blue-bg"></span>
			<span class="separate_border tess-yellow-bg"></span>
			<span class="separate_border tess-orange-bg"></span>
			<span class="separate_border tess-dark-pink-bg"></span>
			<span class="separate_border tess-green-bg"></span>
		</div>

	</article>

</main>
<?php get_footer(); ?>
