<?php get_header(); ?>
<?php get_template_part( 'overlay' ); ?>
<main class="tess-light-pink-bg">
	<div class="container is-fluid">
				<!-- section -->
				<section>

					<?php get_template_part('loop-recipes'); ?>

					<?php get_template_part('pagination'); ?>

				</section>
				<!-- /section -->
	</div><!-- /.container -->
</main>
<?php get_footer(); ?>
