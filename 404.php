<?php get_header(); ?>
<main>
	<article class="vh-100 dt w-100 tess-light-pink-bg">
	  <div class="dtc v-mid tc white ph3 ph4-l">
	    <h1 class="tess-orange">WOOOPS!</h1>
			<p class="tess-light-blue">The page you were looking for was not found, why not head back <a href="/" class="tess-light-blue">home!</a></p>
	  </div>
	</article>
</main>
