<?php get_header(); ?>
<?php get_template_part( 'overlay' ); ?>
<main class="tess-light-pink-bg">
		<!-- section -->
		<section>

			<?php get_template_part('loop-events'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
</main>
<?php get_footer(); ?>
