<div class="w-75 center tc pt6 pb4 terms">

	<div id="filters">
		<?php

			$terms = get_terms( 'genre', array('hide_empty' => 0, 'parent' =>0));
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			    $count = count( $terms );
			    $i = 0;
			    $term_list = '<p class="term-archive">';
			    foreach ( $terms as $term ) {
			        $i++;
			        $term_list .= '<label data-labelfor="red"><input class="terminput" type="checkbox" name="red" value=".red" id="red"' . '" alt="' . '">' . $term->name . '</label>';
			        if ( $count != $i ) {
			            $term_list .= ' <span class="dot-separate tess-light-blue">&middot;</span> ';
			        }
			        else {
			            $term_list .= '</p>';
			        }
			    }
			    echo $term_list;
			}if ( $count != $i ) {
					$term_list .= ', ';
			}
	 ?>
	</div>

 </div>

<div class="recipes" id="recipes">
	<div class="recipe-sizer"></div>
	<div class="recipe-gutter"></div>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<div <?php post_class( 'recipe-item' ); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
					<p class="pb4"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
				</a>
			<?php endif; ?>
			<!-- /post thumbnail -->

		</div>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

	<?php endif; ?>
</div>
