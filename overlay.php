<div id="menu-overlay" class="tess-light-pink-bg">
  <article class="vh-100 dt w-100">
    <!-- mobile header -->
    <header class="fixed top-0 w-100 z-5 hide-desktop tess-light-pink-bg">
      <div class="tess_navigation">
        <div class="cf ph2-ns">
          <div class="fl w-20">
            <div class="item pv3 tc"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/lens.svg" class="js-trigger-overlay"></div>
          </div>
          <div class="fl w-60">
            <div class="pv3 tc logo"></div>
          </div>
          <div class="fl w-20">
            <div class="item pv3 tc"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/x_orange.svg" class="js-trigger-menu burger-menu-close"></div>
          </div>
        </div>
      </div>
    </header>
    <!-- /mobile-header -->
    <div class="dtc v-mid tc white ph3 ph4-l">
      <div class="fl w-100 relative">
        <ul id="horizontal-style">
          <li class="f3"><a href="#">Recipes</a></li>
          <li class="f3"><a href="#">About</a></li>
          <li class="f3"><a href="#">Events</a></li>
          <li class="f3"><a href="#">Contact</a></li>
        </ul>
      </div>
    </div>
  </article>
</div>

<div id="overlay" class="tess-light-pink-bg">
  <article class="vh-100 dt w-100">
    <div class="dtc v-mid tc white ph3 ph4-l">
      <div class="fl w-100 relative">
        <input class="w-80 w-50-ns search-input">
        <input type="submit" class="input-reset w-auto-ns search-submit" value="&#10005;">
      </div>
    </div>
  </article>
</div>
