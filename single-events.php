<?php get_header(); ?>
<?php get_template_part( 'overlay' ); ?>
<main>

	<article class="w-100 tess-light-pink-bg pb4 events_posts_container">
		<div class="container is-fluid">

					<!-- section -->
					<section>

					<?php if (have_posts()): while (have_posts()) : the_post(); ?>

						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<div class="events_cont">
								<div class="row">
								  <div class="col-7">
										<div class="event-content">
											<img src="<?php echo get_template_directory_uri(); ?>/img/2.png" />
											<img src="<?php echo get_template_directory_uri(); ?>/img/3.png" />
											<img src="<?php echo get_template_directory_uri(); ?>/img/4.png" class="lastpic" />
											<img src="<?php echo get_template_directory_uri(); ?>/img/2.png" />
										</div>
									</div>
								  <div class="col-5">
										<div class="stick relative">
											<article class="vh-75 dt w-100">
												<h1 class="tess-yellow the_event_title"><?php the_title(); ?></h1>
												<span class="absolute bottom-0">
													<p class="tess-light-blue">Red Smart Women’s week - a festival of empowerment and inspiration saw chef Tess Ward headline with an exclusive lunch, hosted at one Belgravia Square.</p>
													<p class="tess-light-blue pt5"><a href="/events"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="left_arrow dib"> Back to Events</a></p>
												</span>
											</article>
										</div>
									</div>
								</div>
							</div>



						</article>
						<!-- /article -->

					<?php endwhile; ?>

					<?php else: ?>

						<!-- article -->
						<article>

							<h1><?php _e( 'Sorry, nothing to display.', 'wpbootstrapsass' ); ?></h1>

						</article>
						<!-- /article -->

					<?php endif; ?>

					</section>
					<!-- /section -->
		</div><!-- /.container -->
	</article>

	<?php
	$prev_post = get_previous_post();
	if (!empty( $prev_post )): ?>
	<article class="vh-25 dt w-100 tess-light-pink-bg pb4 next_event">
		<div class="dtc v-mid tc white ph3 ph4-l">
			<div class="container is-fluid">
				<p class="pb2 tess-light-blue">Next Event</p>
				<a class="next_event" href="<?php echo $prev_post->guid ?>"><h3><?php echo $prev_post->post_title ?></h3></a>
				<p class="tess-light-blue pt2"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrow_recipes.svg" class="bottom_arrow dib"></p>
		  </div>
		</div>
	</article>
	<?php else: ?>
		<?php get_footer(); ?>
	<?php endif ?>
</main>
